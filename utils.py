import requests
import asyncio


loop = asyncio.get_event_loop()


async def async_get(url):
    try:
        req = await loop.run_in_executor(None, requests.get, url)
    except Exception:
        return None
    return req


def parse_seconds(seconds, *mods):
    if mods == ():
        return [seconds] + ([0] * len(mods))
    remainder, current = divmod(seconds, mods[0])
    return [current] + parse_seconds(remainder, *mods[1:])
