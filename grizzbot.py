#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import asyncio
import discord
import yaml
import os

from discord.ext import commands


class GrizzBot(commands.Bot):

    modtimes = {}

    def __init__(self, settings, *args, **kwargs):
        self._settings = settings
        super().__init__(*args, **kwargs)

    async def on_ready(self):
        await self.change_presence(
                activity=discord.Game(name='Grabbing times...'))
        await self._reload_loop()

    async def _reload_loop(self):
        while True:
            await self._load_plugins()
            await asyncio.sleep(10)
        return None

    async def _load_plugins(self):
        files = [x for x in os.listdir('plugins') if x.endswith('.py')]
        for file_ in files:
            name, _ = os.path.splitext(file_)

            # unload the changed ones here.
            self.load_extension('plugins.{}'.format(name))
        return None


def main():
    with open('settings.yaml', 'r') as f:
        settings = yaml.load(f.read())

    grizzbot = GrizzBot(settings, '?')
    grizzbot.run(settings['token'])
    return None


if __name__ == "__main__":
    main()
