import time
import asyncio

from discord.ext import commands
from discord.abc import Snowflake


# {guild_id: role_id}
LFG_ID = {
    '424059346645942273': 425419502789066752,  # Grizzbot server
    '423894025934995476': 425342414199455764,  # Grizzco's Elite Forces
}
IDLE_LFG_TIME = 2 * 60 * 60


class Commands:

    _looking_for_games = {}

    def __init__(self, bot):
        self.bot = bot
        asyncio.ensure_future(self._idle_loop())

    async def on_message(self, message):
        user = message.author
        if user in self._looking_for_games:
            _, role = self._looking_for_games[user]
            self._looking_for_games[user] = (time.time(), role)
        return None

    async def _idle_loop(self):
        while True:
            await asyncio.sleep(60)
            await self._remove_idle_lfg()
        return None

    async def _remove_idle_lfg(self):
        new = {}
        current_time = time.time()
        for user, (_time, role) in self._looking_for_games.items():
            if current_time - _time > IDLE_LFG_TIME:
                await user.remove_roles(role)
            else:
                new[user] = (_time, role)
        self._looking_for_games = new
        return None

    @commands.command()
    async def hello(self, ctx):
        await ctx.send('Hello {}.'.format(ctx.author.mention))
        return None

    @commands.command(name='lfg')
    async def games(self, ctx):
        guild = ctx.message.guild
        user = ctx.author
        role = _get_lfg_role(guild)
        print(role, guild.id)
        if role is None:
            return None

        if any(r.id == role.id for r in user.roles):
            del self._looking_for_games[user]
            await user.remove_roles(role)
        else:
            self._looking_for_games[user] = (time.time(), role)
            await user.add_roles(role)
        return None

    @commands.command(hidden=True)
    async def roles(self, ctx):
        guild = ctx.message.guild
        output = []
        for role in guild.roles:
            output.append('{} - {}'.format(role.name, role.id))
        await ctx.send('```{}```'.format('\n'.join(output)))
        return None


def _get_lfg_role(guild, default=None):
    role_id = LFG_ID.get(str(guild.id), None)
    if role_id is None:
        return default
    for role in guild.roles:
        if role_id == role.id:
            return role
    return default


def setup(bot):
    bot.add_cog(Commands(bot))
    return None
