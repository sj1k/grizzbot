import asyncio
import json
import time
import datetime

import discord
from discord.ext import commands

import utils


CHANNELS = [
    424070579793166346,
    425141402561675274,
]

# TODO: Make those addable / removable via a command ^


class Salmon:

    info = None
    online = False
    _notified = False

    def __init__(self, bot):
        self.bot = bot
        asyncio.ensure_future(self._info_loop())
        asyncio.ensure_future(self._update_loop())

    async def _info_loop(self):
        while True:
            self.info = await _get_salmon_info()
            await asyncio.sleep(60 * 60)
        return None

    async def _update_loop(self):
        info = None
        start_time = None
        end_time = None
        while True:
            if info is not None:
                current_time = time.time()
                online = start_time < current_time < end_time
                await self._update_salmon(
                        online, start_time, end_time,
                        current_time)

                if (start_time - current_time < 0
                and not self.online):
                    await self._send_online_notification(
                        online, start_time, end_time, current_time,
                        title='ONLINE NOW!!!')
                    self.online = True

                if (end_time - current_time < 0
                and self.online):
                    self.online = False
                    self._notified = False
                    info = self.info['details'][0]
                    start_time = info['start_time']
                    end_time = info['end_time']
                    await self._send_offline_notification(
                        online, start_time, end_time, current_time)
                await asyncio.sleep(60)
            else:
                if self.info is None:
                    info = None
                else:
                    info = self.info['details'][0]
                    start_time = info['start_time']
                    end_time = info['end_time']
                await asyncio.sleep(10)
        return None

    async def _send_offline_notification(self, _, start, end, current):
        data = self.info['details'][1]
        next_start = data['start_time']
        next_end = data['end_time']
        info = _get_info(self.info['details'][1], next_start, next_end, current)

        embed = discord.Embed(
            title='Salmon run is OFFLINE!!! - Next in {}'.format(info['time']))
        embed.add_field(name='Stage', value=info['stage'])
        embed.add_field(name='Weapons', value=info['weapons'])
        # embed.set_footer(text='Ends in {}'.format(info['ends']))

        for channel in self.bot.get_all_channels():
            if channel.id in CHANNELS:
                await channel.send(embed=embed)
        return None

    async def _send_online_notification(self, online, start, end, current,
                                        title='ONLINE NOW!!!'):
        info = _get_info(self.info['details'][0], start, end, current)

        embed = discord.Embed(title='Salmon run is {}'.format(title))
        embed.add_field(name='Stage', value=info['stage'])
        embed.add_field(name='Weapons', value=info['weapons'])
        embed.set_footer(text='Ends in {}'.format(info['ends']))

        for channel in self.bot.get_all_channels():
            if channel.id in CHANNELS:
                await channel.send(embed=embed)
        return None

    async def _update_salmon(self, online, start, end, current):
        if not online:
            until = start - current
            prefix = 'in'
        else:
            until = end - current
            prefix = 'for'
        output = '{} {}'.format(prefix, _parse_duration(until))
        await self.bot.change_presence(activity=discord.Game(name=output))
        if self.online == online:
            return None
        return None

    @commands.command(aliases=['sr'])
    async def salmonrun(self, ctx):
        if self.info is None:
            await ctx.send('I have not retrieved the info yet, please wait.')
            return None

        await ctx.trigger_typing()
        info = self.info['details'][0]
        start_time = info['start_time']
        end_time = info['end_time']
        current_time = time.time()
        sr_info = _get_info(info, start_time, end_time, current_time)

        if start_time < current_time < end_time:
            footer = 'Ends in {}'.format(sr_info['ends'])
        else:
            footer = 'Runs for {}'.format(sr_info['duration'])

        embed = discord.Embed(title=sr_info['title'])
        embed.add_field(name='Stage', value=sr_info['stage'])
        embed.add_field(name='Weapons', value=sr_info['weapons'])
        embed.set_footer(text=footer)

        await ctx.send(embed=embed)
        return None


def _get_info(info, start_time, end_time, current_time):
    online = start_time < current_time < end_time
    online_str = 'ONLINE!' if online else 'OFFLINE!'
    start_ends = 'ENDS' if online else 'STARTS'
    weapons = '\n'.join(w['name'] for w in info['weapons'])
    stage = info['stage']['name']
    if not online:
        duration = start_time - current_time
    else:
        duration = end_time - current_time
    time_str = _parse_duration(duration)
    clock_icon = _get_clock_icon(duration)
    title = 'Salmon run is {} - {} in {} {}'.format(
        online_str, start_ends, time_str, clock_icon)
    end_str = _parse_duration(end_time - current_time)
    duration_str = _parse_duration(end_time - start_time)
    info = {'title': title, 'weapons': weapons, 'stage': stage,
            'clock_icon': clock_icon, 'time': time_str, 'ends': end_str,
            'duration': duration_str}
    return info


def _parse_duration(duration):
    parsed = utils.parse_seconds(duration, 60, 60, 24)
    seconds, minutes, hours, days = list(map(round, parsed))
    if days != 0:
        days_in = '{}d '.format(days)
    else:
        days_in = ''
    return '{}{}h {}m'.format(days_in, hours, minutes)


def _get_clock_icon(duration):
    parsed = utils.parse_seconds(duration, 60, 60, 24)
    seconds, minutes, hours, days = list(map(round, parsed))
    now = datetime.datetime.utcnow()
    then = now + datetime.timedelta(days=days, hours=hours, minutes=minutes,
                                    seconds=seconds)
    then += datetime.timedelta(minutes=15)
    then -= datetime.timedelta(minutes=then.minute % 30,
                               seconds=then.second,
                               microseconds=then.microsecond)
    _hour = then.hour % 12 or 12
    _minute = then.minute if then.minute != 0 else ''
    return f':clock{_hour}{_minute}:'


async def _get_salmon_info():
    url = 'https://splatoon2.ink/data/coop-schedules.json'
    req = await utils.async_get(url)
    data = json.loads(req.text)
    return data


def setup(bot):
    bot.add_cog(Salmon(bot))
    return None
